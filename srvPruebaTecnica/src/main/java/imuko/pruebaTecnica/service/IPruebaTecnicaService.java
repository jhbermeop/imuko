package imuko.pruebaTecnica.service;

import imuko.pruebaTecnica.commons.ArchivoDTO;

public interface IPruebaTecnicaService {

    String guardarArchivo(ArchivoDTO json) throws Exception;

    String listarArchivos() throws Exception;


}
