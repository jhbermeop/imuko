package imuko.pruebaTecnica.service;

import imuko.pruebaTecnica.commons.ArchivoDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Base64;

@Service
public class PruebaTecnicaService implements IPruebaTecnicaService {

    @Autowired
    private Environment environment;

    @Override
    public String guardarArchivo(ArchivoDTO json) throws Exception {
        try {

            String ruta = environment.getProperty("rutaArchivo");
            System.out.println("esta es la ruta  " + ruta);

            String contenido = json.getText();
            String nombre = json.getFilename();

            File archivo = new File(ruta + nombre);
            byte[] bytes = Base64.getDecoder().decode(contenido);

            FileOutputStream outputStream = new FileOutputStream(ruta + nombre);
            outputStream.write(bytes);

            outputStream.close();

            return "se creo el archivo en la ruta: " + ruta;

        } catch (Exception e) {
            return "ocurrio un error al crear el archivo";
        }
    }


    @Override
    public String listarArchivos() throws Exception {
        try {
            String ruta = environment.getProperty("rutaArchivo");
            File folder = new File(ruta);
            ArrayList<String> lista = new ArrayList<>();
            for (final File fileEntry : folder.listFiles()) {
                lista.add(fileEntry.getName());
                System.out.println(fileEntry.getName());
            }
            System.out.println(lista);

            return lista.toString();

        } catch (Exception e) {
            return "ocurrio un error al listar los archivos";
        }
    }
}
