package imuko.pruebaTecnica.web.api.rest;


import imuko.pruebaTecnica.commons.ArchivoDTO;
import org.springframework.web.bind.annotation.RequestBody;

import javax.validation.Valid;

public interface IPruebaTecnicaController {

    String guardarArchivo(@Valid @RequestBody ArchivoDTO json) throws Exception;

    String listarArchivos() throws Exception;

}
