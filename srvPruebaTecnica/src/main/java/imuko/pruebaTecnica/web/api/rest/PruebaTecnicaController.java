package imuko.pruebaTecnica.web.api.rest;

import com.fasterxml.jackson.databind.ObjectMapper;
import imuko.pruebaTecnica.commons.ArchivoDTO;
import imuko.pruebaTecnica.service.IPruebaTecnicaService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponses;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;

@RestController
@RequestMapping("/v1")
@CrossOrigin
public class PruebaTecnicaController implements IPruebaTecnicaController {

    private static final Logger logger = LoggerFactory.getLogger(PruebaTecnicaController.class);

    private ObjectMapper mapper = new ObjectMapper();

    @Autowired
    private IPruebaTecnicaService categoriaService;

    @Override
    @PostMapping(path = "/guardarArchivo", produces = "application/json", consumes = "application/json")
    @ApiOperation(value = "Crea un archivo en el sistema", notes = "notas")
    public String guardarArchivo(@Valid @RequestBody @ApiParam(type = "ArchivoDTO", value = "{\n" +
            "  \"text\": \"BASE64\",\n" +
            "  \"filename\": \"IMUKO\"\n" +
            "}", required = true) ArchivoDTO json) throws Exception {
        try {

            String genericResponseDTO = categoriaService.guardarArchivo(json);

            return "Se creo correctamente el archivo";

        } catch (ResponseStatusException | HttpClientErrorException e) {
            logger.error(e.getMessage());
            throw new ResponseStatusException(HttpStatus.NOT_FOUND,
                    "No se encuentra informacion");

        } catch (Exception e) {
            logger.error(Thread.currentThread().getStackTrace()[1].getMethodName(), e);
            throw new ResponseStatusException(HttpStatus.NOT_FOUND,
                    "Ocurrio un error inesperado");
        }
    }


    @Override
    @GetMapping(path = "/listarArchivos", produces = "application/json")
    @ApiOperation(value = "Consultar todos los archivos en el sistema", notes = "notas")
    public String listarArchivos() throws Exception {
        try {

            String genericResponseDTO = categoriaService.listarArchivos();

            return "Se listaron correctamente los archivos: "+genericResponseDTO;

        } catch (ResponseStatusException | HttpClientErrorException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND,
                    "No se encuentra informacion");
        } catch (Exception e) {
            logger.error(Thread.currentThread().getStackTrace()[1].getMethodName(), e);
            throw new ResponseStatusException(HttpStatus.NOT_FOUND,
                    "Ocurrio un error inesperado");
        }
    }

}
